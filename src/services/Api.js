import Vue from 'vue'
import Axios from 'axios'

Vue.prototype.$axios = Axios
Axios.defaults.baseURL = 'http://localhost:8000'
Axios.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded'