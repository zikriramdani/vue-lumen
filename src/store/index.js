import Vue from 'vue'
import Api from '../services/Api'
import Vuex from 'vuex'
import 'es6-promise/auto'
Vue.use(Vuex)

import alert from './alert'
import post from './modules/post'

// const debug = 'development';
const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
	modules: {
		alert,
		post
	},
    strict: debug,
});


if (module.hot) {
  module.hot.accept([
  	'./alert',
    './modules/post'
  	], () => {

  	const alert = require('./alert').default;
  	const post = require('./modules/post').default;

    store.hotUpdate({
		modules: {
			post,
			alert
		},
    })
  })
}