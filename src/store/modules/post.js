import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'
import 'es6-promise/auto'
Vue.use(Vuex)

const state = () => ({
	post: {},
});

const actions = {
    LOAD_PROJECT_LIST: function ({ commit }) {
        axios.get('/post').then((response) => {
            // console.log("success")
          commit('SET_PROJECT_LIST',
        {
          dataPost: response.data
        })
        }, (err) => {
          console.log(err)
        })
    }
}

const mutations = {
  SET_PROJECT_LIST: (state, { dataPost }) => {
        // console.log(dataPost)
        state.post = dataPost
    },
}

const getters = {
}

export default {
	state,
	getters,
	actions,
	mutations,
};